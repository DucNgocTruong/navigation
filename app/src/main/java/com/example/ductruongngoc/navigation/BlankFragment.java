package com.example.ductruongngoc.navigation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.navigation.Navigation;

public class BlankFragment extends Fragment {

    EditText edtName, edtAge;
    RelativeLayout rlAll;
    Button btnNext;


    public BlankFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnNext = getView().findViewById(R.id.btn_next);

        edtName = getView().findViewById(R.id.edt_name);
        edtAge = getView().findViewById(R.id.edt_age);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BlankFragmentDirections.ActionBlankFragmentToBlankFragment2 action = BlankFragmentDirections.actionBlankFragmentToBlankFragment2(new peopleModel(edtName.getText().toString(), edtAge.getText().toString()));
                Navigation.findNavController(v).navigate(action);
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blank, container, false);
    }


}
