package com.example.ductruongngoc.navigation;

import android.os.Parcel;
import android.os.Parcelable;

public class peopleModel implements Parcelable {

    String name;
    String age;

    public peopleModel(String name, String age) {
        this.name = name;
        this.age = age;
    }

    protected peopleModel(Parcel in) {
        name = in.readString();
        age = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(age);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<peopleModel> CREATOR = new Creator<peopleModel>() {
        @Override
        public peopleModel createFromParcel(Parcel in) {
            return new peopleModel(in);
        }

        @Override
        public peopleModel[] newArray(int size) {
            return new peopleModel[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}
